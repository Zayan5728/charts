import React from 'react';
import { Link, Route, Routes } from 'react-router-dom';
import Home from './Home';
import Chartform from './Components/Chartform';
import Existingcharts from './Components/Existingcharts';
import Newcharts from './Components/NewCharts';
import Editchart from './Components/Editchart';
const Headers = () => {
  return (
    <div className="flex space-x-6">
      <div className="flex flex-col w-52  bg-slate-500 space-y-8">
        <Link to="/">
          <button className="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 w-32 py-2.5 mb-2 dark:focus:ring-yellow-900">Dashboard</button>
        </Link>
        <Link to="/view">
          <button className="focus:outline-none text-white bg-yellow-400 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 w-32 dark:focus:ring-yellow-900">View</button>
        </Link>
      </div>
      <div className='space-x-8'>
        <div className='flex  gap-8'>
          <div className='mr-2px'>
            <Link to="/Existingcharts">
              <button className="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-red-600 dark:hover:bg-red-700 w-32 dark:focus:ring-red-900">Existingcharts</button>
            </Link></div>
          <div>
            <Link to="/Newcharts">
              <button className="focus:outline-none text-white bg-green-700 hover-bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 dark:bg-green-600 w-32 dark:hover-bg-green-700 dark:focus-ring-green-800">Newcharts</button>
            </Link>
          </div>
        </div>
        <div className="mt-8">
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/view" element={<Chartform />} />
            <Route path="/Existingcharts" element={<Existingcharts />} />
            <Route path="/Newcharts" element={<Newcharts/>} />
            <Route path="/Existingcharts/Editchart" element={<Editchart />} />
          </Routes>
        </div>
      </div>

    </div>
  );
};

export default Headers;
