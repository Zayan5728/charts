import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ChartProvider } from './Components/Chartcontext';
ReactDOM.render(
  <React.StrictMode>
    <ChartProvider>
      <App />
    </ChartProvider>
  </React.StrictMode>,
  document.getElementById('project')
);
