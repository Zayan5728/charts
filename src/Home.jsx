import React from 'react';
import { Table  } from 'antd';
const { Column,ColumnGroup} = Table;
const data = [
  {
    key: '1',
    Name: 'Bar chart',
    Type: 'Coloumn chart',
    ChartDescription: 'A bar chart or bar graph is a chart or graph that presents categorical data with rectangular bars with heights or lengths proportional to the values that they represent',
  },
  {
    key: '2',
    Name: 'Line Chart',
    Type: 'Linear Chart',
    ChartDescription: 'A line chart is a type of chart used to show information that changes over time.Line charts are created by plotting a series of several points and connecting them with a straight line.',
  },
  {
    key: '3',
    Name: 'Pie Chart',
    Type: 'Circular Chart',
    ChartDescription:'Pie chart sometimes called a circle chart, is a way of summarizing a set of nominal data or displaying the different values of a given variable.'
  },
  {
    Key:'4',
    Name: 'Polar area Chart',
    Type: 'Circular Chart',
    ChartDescription:'Polar charts are a form of graph that allows a visual comparison between several quantitative or qualitative aspects of a situation.'
    
  }
];
const Home = () => (
  <div style={{height:'550px', width:'800px'}} className='ml-20 mt-4 mb-6'>
    <Table dataSource={data}>
      <ColumnGroup title="Chart">
      <Column title="Name" dataIndex="Name" key="Name" />
      <Column title="Type" dataIndex="Type" key="Type" />
      </ColumnGroup>
      <Column title="Chart Description" dataIndex="ChartDescription" key="ChartDescription" />
    </Table>
  </div>
  );
export default Home;