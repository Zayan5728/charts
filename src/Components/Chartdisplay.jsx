import React, { useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';
import { BarController, LineController, PieController, PolarAreaController, RadarController, ScatterController, BubbleController } from 'chart.js'
Chart.register(BarController, LineController, PieController, PolarAreaController, RadarController, ScatterController, BubbleController);
const ChartDisplay = ({ chartType, existingData }) => {
  const chartRef = useRef(null);
  const chartInstance = useRef(null);

  useEffect(() => {
    if (chartRef.current) {
      const ctx = chartRef.current.getContext('2d');

      if (chartInstance.current) {
        chartInstance.current.destroy();
      }

      const data = {
        labels: existingData.data.series, 
        datasets: [
          {
            label: existingData.data.datasets[0].label,
            data: existingData.data.datasets[0].categories, 
            backgroundColor: existingData.data.datasets[0].backgroundColor,
            borderWidth: existingData.data.datasets[0].borderWidth,
          },
        ],
      };

      const options = existingData.options;

      if (chartType === 'polar') {
        chartInstance.current = new Chart(ctx, {
          type: 'polarArea',
          data,
          options,
        });
      } else {
        chartInstance.current = new Chart(ctx, {
          type: chartType,
          data,
          options,
        });
      }
    }
  }, [chartType, existingData]);

  return (
    <div style={{ height: '550px', width: '1000px' }}>
      <canvas ref={chartRef}></canvas>
    </div>
  );
};

export default ChartDisplay;
