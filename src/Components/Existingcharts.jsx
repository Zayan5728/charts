import React, { useState } from 'react';
import { Table, Button } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
// import Editchart from './Editchart';
import { useNavigate } from 'react-router-dom';
const Existingcharts = () => {
  const navigate=useNavigate()
  // const confirm = window.confirm('are you sure')
  const [availableCharts, setAvailablecharts] = useState([
    {
      key: '1',
      serialNumber: 1,
      chartName: 'Bar chart',
      tags:['Edit','Delete']
    },
    {
      key: '2',
      serialNumber: 2,
      chartName: 'Pie chart',
      tags:['Edit','Delete']
    },
    {
      key: '3',
      serialNumber: 3,
      chartName: 'line  chart',
      tags:['Edit','Delete']

    },
    {
      key: '4',
      serialNumber: 4,
      chartName: 'polar  chart',
      tags:['Edit','Delete']

    },
    {
      key: '5',
      serialNumber: 5,
      chartName: 'scatter  chart',
      tags:['Edit','Delete']

    }
  ]);

  
  const columns = [
    {
      title: 'Serial Number',
      dataIndex: 'serialNumber',
      key: 'serialNumber',
    },
    {
      title: 'Chart Name',
      dataIndex: 'chartName',
      key: 'chartName',
    },
    {
      title: 'Actions',
      key: 'actions',
      render: (text, record) => (
        <span>
          <Button
            type="default"
            icon={<EditOutlined/>}
            onClick={()=>{navigate('./Editchart')}}
            className='hover:bg-blue-300'
          >
            Edit
          </Button>
          <Button
            type="danger"
            icon={<DeleteOutlined/>}
            onClick={()=>{handleDelete(record)}}
            className='hover:bg-red-500'
          
          >
            Delete
          </Button>
        </span>
      ),
    },
  ];
  
  const handleDelete = (record) => {
    const shouldDelete = window.confirm('Are you sure you want to delete this chart?');
    if (shouldDelete) {
      const updatedCharts = availableCharts.filter((chart) => chart.key !== record.key);
      setAvailablecharts(updatedCharts);
    }
  }

  return (
    <div style={{height:'550px', width:'800px'}} className='ml-20 mt-16'>
      <Table dataSource={availableCharts} columns={columns}  Delete={handleDelete}/>
    </div>
  );
};

export default Existingcharts;
