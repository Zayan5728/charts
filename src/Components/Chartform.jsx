import React, { useState } from 'react';
import { useChartContext } from './Chartcontext';
import ChartDisplay from './Chartdisplay'
const ChartForm = () => {
  const { state } = useChartContext(); 
  const [chartType, setChartType] = useState('bar'); 
  console.log('before adding',state);
  console.log('after adding',state)

  const changeChartType = (e) => {
    setChartType(e.target.value);
  };

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  return (
    <div>
      <form>
        <label>
          <select
            className="p-2 rounded bg-blue-400 text-white"
            value={chartType}
            onChange={changeChartType}
          >
            {state.availableChartTypes.map((chartType) => (
              <option key={chartType} value={chartType}>
                {capitalizeFirstLetter(chartType)} Chart
              </option>
            ))}
          </select>
        </label>
      </form>

      <div className="flex">
        <div className="flex-1">
          <ChartDisplay chartType={chartType} existingData={state} />
        </div>
      </div>
    </div>
  );
};

export default ChartForm;
