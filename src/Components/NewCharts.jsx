
import React, { useState } from 'react';
import { useChartContext } from './Chartcontext';

const NewCharts = () => {
  const { addChartType,addNewData, state } = useChartContext();

  const [newdata, setNewData] = useState({
    label: '',
    categories: '',
    series: '',
    legend: '',
    chartType: '',
  });

  const change = (evt) => {
    setNewData({ ...newdata, [evt.target.name]: evt.target.value });
  };

  const save = (evt) => {
    evt.preventDefault();

    const { label, categories, series, legend, chartType } = newdata;

    const dataset = {
      label,
      categories: categories.split(',').map((item) => item.trim()),
    };

    if (series) {
      dataset.series = series.split(',').map((item) => item.trim());
    }

    const newData = {
      data: {
        datasets: [dataset],
      },
      options: {
        legend,
        chartType,
      },
    };

    addNewData(newData);

    if (!state.availableChartTypes.includes(chartType)) {
      addChartType(chartType);
    }

    setNewData({
      label: '',
      categories: '',
      series: '',
      legend: '',
      chartType: '',
    });
  };

  return (
    <div style={{ height: '550px', width: '500px' }} className="mt-12 mb-1 bg-gray-200 p-4 rounded-lg shadow-md ml-32 w-96">
      <div className="space-y-4">
        <div>
          <label className="text-sm font-semibold">Label:</label>
          <input
            type="text"
            name="label"
            value={newdata.label}
            onChange={change}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">x-axis(categories)</label>
          <input
            type="text"
            name="categories"
            value={newdata.categories}
            onChange={change}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">y-axis(series)</label>
          <input
            type="text"
            name="series"
            value={newdata.series}
            onChange={change}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">Legend:</label>
          <input
            type="text"
            name="legend"
            value={newdata.legend}
            onChange={change}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">Chart Type:</label>
          <input
            type="text"
            name="chartType"
            value={newdata.chartType}
            onChange={change}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <button
          onClick={save}
          className="bg-blue-500 text-white px-4 py-2 rounded-md hover-bg-blue-600 transition duration-200"
        >
          Save
        </button>
      </div>
    </div>
  );
};

export default NewCharts;
