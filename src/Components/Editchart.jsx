import React, { useState } from 'react';

const Editchart = ({ onEdit }) => {
  const [label, setLabel] = useState('');
  const [xaxis, setXaxis] = useState('');
  const [yaxis, setYaxis] = useState('');
  const [legend, setLegend] = useState('');
  const [chartType, setChartType] = useState('');

  const save = () => {
    const editedData = {
      series: [
        {
          data: [yaxis],
        },
      ],
      options: {
        chart: {
          type: chartType,
          height: 350,
        },
        plotOptions: {
          bar: {
            borderRadius: 4,
            horizontal: true,
          },
        },
        dataLabels: {
          enabled: false,
        },
        labels: {
          label: label,
        },
        xaxis: {
          categories: [xaxis],
        },
        legend: {
          show: legend,
        },
      },
    };
    onEdit(editedData);
  };

  return (
    <div style={{ height: '550px', width: '500px' }} className="mt-12 mb-1 bg-gray-200 p-4 rounded-lg shadow-md ml-32 w-96">
      <div className="space-y-4">
        <div>
          <label className="text-sm font-semibold">Label:</label>
          <input
            type="text"
            value={label}
            onChange={(e) => setLabel(e.target.value)}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">x-axis:</label>
          <input
            type="text"
            value={xaxis}
            onChange={(e) => setXaxis(e.target.value)}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">y-axis:</label>
          <input
            type="text"
            value={yaxis}
            onChange={(e) => setYaxis(e.target.value)}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">Legend:</label>
          <input
            type="text"
            value={legend}
            onChange={(e) => setLegend(e.target.value)}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <div>
          <label className="text-sm font-semibold">Chart Type:</label>
          <input
            type="text"
            value={chartType}
            onChange={(e) => setChartType(e.target.value)}
            className="px-4 py-2 border rounded-md w-full"
          />
        </div>
        <button
          onClick={save}
          className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 transition duration-200"
        >
          Save
        </button>
      </div>
    </div>
  );
};

export default Editchart;
