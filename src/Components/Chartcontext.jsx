// ChartContext.js
import React, { createContext, useContext, useReducer } from 'react';

const ChartContext = createContext();

export const useChartContext = () => useContext(ChartContext);

const initialState = {
  type: ['bar', 'line', 'pie', 'polar', 'radar', 'scatter', 'bubble'],
  data: {
    series: ['us', 'pakistan', 'unitedkingdum', 'germany', 'france', 'ireland', 'brazil', 'canada', 'singapore', 'india'],
    datasets: [
      {
        label: 'Population in million',
        categories: [400, 430, 600, 800, 1000, 1200, 1600, 2000, 2500, 3200],
        borderWidth: 1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
    legend: {
      show: true,
    },
    animation: {
      duration: 5000,
      easing: 'linear',
    },
  },
  availableChartTypes: ['bar', 'line', 'pie', 'polar', 'radar', 'scatter', 'bubble'],
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'ADD_NEW_DATA':
      if (state.availableChartTypes.includes(action.payload.chartType)) {
        return {
          ...state,
          data: {
            ...state.data,
            datasets: [
              ...state.data.datasets,
              {
                label: action.payload.label,
                categories: action.payload.categories,
                borderWidth: 1,
              },
            ],
          },
          options: {
            legend: {
              show: action.payload.legend,
            },
          },
        };
      }
      return state;

    case 'ADD_CHART_TYPE':
      return {
        ...state,
        availableChartTypes: [...state.availableChartTypes, action.payload],
      };

    default:
      return state;
  }
};

export const ChartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const addNewData = (newData) => {
    dispatch({ type: 'ADD_NEW_DATA', payload: newData });
  };

  const addChartType = (chartType) => {
    dispatch({ type: 'ADD_CHART_TYPE', payload: chartType });
  };

  return (
    <ChartContext.Provider value={{ state, dispatch, addNewData, addChartType }}>
      {children}
    </ChartContext.Provider>
  );
};
