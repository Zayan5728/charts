import React from 'react';
import './index.css'
import { BrowserRouter } from 'react-router-dom';
import Headers from './Headers';
function App() {

  return (
    <div className="bg-purple-400" >
      <BrowserRouter>
        <Headers />
      </BrowserRouter>
    </div>
  )

}

export default App;
